package com.example.user.slidingtest;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by user on 5/22/2017.
 */

public class RvHolder extends RecyclerView.ViewHolder {

    TextView textView;

    public RvHolder(View itemView) {
        super(itemView);
        textView = (TextView) itemView.findViewById(R.id.txt_custom_row);
    }
}
