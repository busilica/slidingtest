package com.example.user.slidingtest;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by user on 5/22/2017.
 */

public class RvAdapter extends RecyclerView.Adapter<RvHolder> {

    Activity activity;

    public RvAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public RvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_row, parent, false);
        return new RvHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RvHolder holder, final int position) {
        holder.textView.setText("TextView " + position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "click", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }
}
