package com.example.user.slidingtest;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,                    /* Host activity */
                drawerLayout,            /* DrawerLayout object */
                R.string.drawer_open,    /* "open drawer" description */
                R.string.drawer_close){  /* "close drawer" description */
            //Called when a drawer has settled in a completely closed state

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //getActionBar().setTitle();
                invalidateOptionsMenu();//creates call to onPrepareOptionsMenu()
            }

            //Called when a drawer has settled in a completely open state

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle();
                invalidateOptionsMenu();//creates call to onPrepareOptionsMenu()
            }
        };

        drawerLayout.setDrawerListener(toggle);

        //drawer
        RecyclerView rv = (RecyclerView) findViewById(R.id.rv_drawer);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(new RvAdapter(this));

        //share
        Button btn = (Button) findViewById(R.id.btn_main);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);  //This flag clears the called app from the activity stack, so users arrive in the expected place next time this application is restarted.
                share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
                share.putExtra(Intent.EXTRA_TEXT, "http://www.codeofaninja.com");
                startActivity(Intent.createChooser(share, "Share link"));
            }
        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //boolean drawerOpen = drawerLayout.isDrawerOpen()
        return super.onPrepareOptionsMenu(menu);
    }
}
